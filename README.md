# GSS README -- Important introductory notes.

GNU Generic Security Service Library implements the GSS-API framework
and a Kerberos V5 mechanism.  GSS-API and Kerberos V5 is used by
network clients and servers to perform authentication, often using the
SSH protocol for remote access or SMTP/IMAP/POP3 for email.

The [GNU GSS web page](https://www.gnu.org/software/gss/) provides
current information about the project.

# Support

If you need help to use Generic Security Service, or wish to help
others, you are invited to join our mailing list help-gss@gnu.org, see
<https://lists.gnu.org/mailman/listinfo/help-gss>.

----------------------------------------------------------------------
Copyright (C) 2003-2022 Simon Josefsson

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.
